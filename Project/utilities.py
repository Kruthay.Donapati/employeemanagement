from .models import *
def TechSearch(keyword):
    techList = Technologies.objects.all()
    if keyword in techList:
        return Technologies.objects.get(name=keyword)
    return None
def ProjectSearch(keyword):
    projectList = Project.objects.all()
    if keyword in projectList:
        return Project.objects.get(name=keyword)
    return None
def KeySearch(keyword):
    if TechSearch(keyword) != None:
        return TechSearch(keyword)
    elif ProjectSearch(keyword) != None:
        return ProjectSearch(keyword)
        
