from django.contrib import admin

from .models import *

admin.site.register(Technologies)
admin.site.register(Employee)
admin.site.register(Manager)
admin.site.register(Project)