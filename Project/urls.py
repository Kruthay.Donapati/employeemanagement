from django.urls import path
from . import views

urlpatterns = [
    path('',views.UserAuth, name ="UserLogin"),
    path('Technologies/',views.AllTechnologies, name = 'ShowTechnologies'),
    path('addproject/',views.AddProject, name = "NewProject"),
    path('User/', views.UserAuth, name = "UserLogin"),
    path('Home/', views.Home, name = "Homepage"),
    path('Project/',views.AllProjects, name = "Projects"),
    path('Employee/',views.AllEmployees, name = "Employees"),
    path('Manager/',views.AllManagers, name = "Managers")

]