from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import *
from django.contrib.auth import authenticate, login
from django import forms
from .forms import *
from django.shortcuts import redirect
from .utilities import *

def UserAuth(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request,
        username = username,
        password = password)
        if user is not None:
            login(request, user)
        
            return redirect('/Home/')
        else:
            return render(request,'Project/login.html')
    else:
        form = LoginForm()
        return render(request,
        'Project/login.html',
        {'form':form}
        )


def Home(request):
    if request.user.is_authenticated:
        form = SearchForm()
        return render(request, 
                'Project/Home.html', 
                {'form': form}
                )
    else:
        return redirect('/User/')

    

def AllTechnologies(request):
    if not request.user.is_authenticated:
        return redirect('/User/')

    
    techList = Technologies.objects.all()
    return render(request,
    'Project/TechList.html',
    {'techList': techList}
    )

def AllProjects(request):
    if not request.user.is_authenticated:
        return redirect('/User/')
    projectList = Project.objects.all()
    return render(request,
    'Project/ProjectList.html',
    {'projectList': projectList}
    )

def AllEmployees(request):
    if not request.user.is_authenticated:
        return redirect('/User/')
    employeeList = Employee.objects.all()
    return render(request,
    'Project/EmployeeList.html',
    {'employeeList': employeeList}
    )

def AllManagers(request):
    if not request.user.is_authenticated:
        return redirect('/User/')
    managerList = Manager.objects.all()
    return render(request,
    'Project/ManagerList.html',
    {'managerList': managerList}
    )

def AddProject(request):
    if not request.user.is_authenticated:
        return redirect('/User/')
    submitted = False
    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/AddProject/?submitted=True')
    else:
        form = ProjectForm()
        if 'submitted' in request.GET:
            submitted = True
    return render(request,
            'Project/AddProject.html',
            {'form': form, 'submitted': submitted}
            )