from django.db import models

class User(models.Model):
    username = models.CharField(max_length = 100)
    password = models.CharField(max_length = 50)

class Technologies(models.Model):
    technologyName = models.CharField("Name of the technology", max_length = 120)
    
    def __str__(self):
        return self.technologyName

class Employee(models.Model):
    firstName = models.CharField('Employee First Name', max_length = 120)
    lastName = models.CharField('Employee Last Name', max_length = 120)
    email = models.EmailField("Employee email id")
    technoligiesKnown = models.ManyToManyField(Technologies)
    
    def __str__(self):
        return self.firstName + " " + self.lastName

class Manager(models.Model):
    firstName = models.CharField('Manager First Name', max_length = 120)
    lastName = models.CharField('Manager Last Name', max_length = 120)
    email = models.EmailField("Manager email id")
    subordinateEmployees = models.ManyToManyField(Employee)
    technoligiesKnown = models.ManyToManyField(Technologies)
    
    def __str__(self):
        return self.firstName + " " + self.lastName

class Project(models.Model):
    name = models.CharField('Project Name', max_length = 120)
    projectStartDate = models.DateTimeField('Project Start Date')
    projectEndDate = models.DateTimeField('Project End Date')
    projectStatus = models.CharField('Project Status', max_length = 120) #ongoing or finished
    manager = models.ForeignKey(Manager, blank = True, null = True, on_delete = models.CASCADE)
    employee = models.ManyToManyField(Employee)
    projectDescription = models.TextField(blank = True)
    TechnologiesRequired = models.ManyToManyField(Technologies)
    
    def __str__(self):
        return self.name    