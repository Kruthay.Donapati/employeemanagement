from django import forms
from django.forms import ModelForm
from .models import *

class ProjectForm(ModelForm):
    require_css_class = 'required'
    class Meta:
        model = Project
        fields = '__all__'

class LoginForm(forms.ModelForm):
    username = forms.CharField(label = "Username")
    password = forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model= User
        fields=[
           'username',
           'password'
            ]
        widgets = {
        'password': forms.PasswordInput()
        }

class SearchForm(forms.Form):
    search = forms.CharField(label = "")